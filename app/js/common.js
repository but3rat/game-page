$( document ).ready(function() {
    $('.home-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrow: false,
        appendArrows: $('.home-slick-box'),
        nextArrow: "<button type = \"button\" class = \"slick-next home-slick-box_next\">See Next</button>",
        prevArrow: "<button type = \"button\" class = \"slick-prev home-slick-box_prev\">See Previous</button>"
    });
    if($(window).width() < 960){
        $('#my-slider').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            appendArrows: $('.home-mobile-slider'),
            nextArrow: "<button type = \"button\" class = \"slick-next home-mobile-slider_next\">See Next</button>",
            prevArrow: "<button type = \"button\" class = \"slick-prev home-mobile-slider_prev\">See Previous</button>"
        });
    }

    $( window ).resize(function() {
        if($(window).width() < 960){
            if(!$('#my-slider').hasClass('slick-slider')){
                $('#my-slider').slick({
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    appendArrows: $('.home-mobile-slider'),
                    nextArrow: "<button type = \"button\" class = \"slick-next home-mobile-slider_next\">See Next</button>",
                    prevArrow: "<button type = \"button\" class = \"slick-prev home-mobile-slider_prev\">See Previous</button>"
                });
            }
        } else{
            if($('#my-slider').hasClass('slick-slider')){
                $('#my-slider').slick('unslick');
            }
        }
    });

    function mmenuToggle(menu, icon){
        var $menu = menu.mmenu({
            navbar: {
                title: '',
            },
            "navbars": [
                {
                    "position": "top",
                    "content": [
                        "<span class='mobile-menu__closer' uk-icon='icon: close; ratio: 1.2'></span>",
                    ]
                }
            ]
        });
        var $icon = icon;
        var closer = $('.mobile-menu__closer');
        var api = $menu.data( "mmenu" );
        $icon.on( "click", function() {
            api.open();
        });

        closer.on( "click", function() {
            api.close();
        });
        api.bind( "close:start", function() {
            $('body').removeClass('menu-open');
            $icon.removeClass( "is-active" );
            $icon.one( "click", function() {
                api.open();
            });
        });
    }

    mmenuToggle($('#menu'), $('#mmenu-icon'))

});

